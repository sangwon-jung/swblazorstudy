﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Customers
{
    public partial class Edit
    {
        [Parameter]
        public int CustomerId { get; set; }
    }
}
