﻿using BlazorInputFile;
using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Services
{
    public interface IFileUploadService
    {
        Task UploadAsync(IFileListEntry file);
    }

}
