﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorApp.Modles
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool  IsDone { get; set; }
    }
}
